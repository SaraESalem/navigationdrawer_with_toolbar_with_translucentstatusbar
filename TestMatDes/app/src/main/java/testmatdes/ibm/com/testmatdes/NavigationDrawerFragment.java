package testmatdes.ibm.com.testmatdes;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
//Navigation Drawer Class
public class NavigationDrawerFragment extends Fragment {

    //This class provides an easy way to tie together the functionality of DrawerLayout and the framework ActionBar to implement the recommended design
    // for navigation drawers.
    private ActionBarDrawerToggle actionBarDrawerToggle; //icon will open navigation drawer
    private DrawerLayout mdrawerLayout;
    private View containerView;

    private boolean mUserLearnedDrawer; //user open/close nav drawer
    private boolean mFromSavedInstanc;
    public static final String PREF_FILE_NAME = "testpref";
    public static final String KEY_USER_LEARNED_DRAWER = "user_learned_drawer";

    public NavigationDrawerFragment() {
        // Required empty public constructor
    }

    //to read value from shared preferences
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //boolean to open/close nav drawer
        //Boolean.valueOf convert from string to boolean
        mUserLearnedDrawer = Boolean.valueOf(readFromPreferences(getActivity(), KEY_USER_LEARNED_DRAWER, "false")); //false mean usr dont open the drawer

        if (savedInstanceState != null) { //come from rotation//rotation happen
            mFromSavedInstanc = true;

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
    }


    //join btwn DrawerLayout & Toolbar //listener on nav drawer open / close
    public void setUp(int fragmentId, DrawerLayout drawerLayout, final Toolbar toolbar) {
        mdrawerLayout = drawerLayout;
        containerView = getActivity().findViewById(fragmentId);

        actionBarDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.open, R.string.close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (!mUserLearnedDrawer) { //if user dont close it and still open
                    mUserLearnedDrawer = true;
                    saveToPreferences(getActivity(), KEY_USER_LEARNED_DRAWER, mUserLearnedDrawer + "");
                }
                getActivity().invalidateOptionsMenu(); //to redraw menu action bar for activity
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                //invalidateOptionsMenu() is used to say Android, that contents of menu have changed, and menu should be redrawn.
                getActivity().invalidateOptionsMenu();
            }

            //to make animation when drawing nav drawer
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {//slideOffset range from 1 to 0 when pull nav drawer to the end increase untill
                //become 1 and opposit in pull opposite become 0
               // super.onDrawerSlide(drawerView, slideOffset);
                if(slideOffset <0.6) { //will make toolbar not complettly darker less
                    toolbar.setAlpha(1 - slideOffset); //make toolbar darker with pulling nav bar
                }
            }
        };
        //if the user has ever seen the drawer //not open or saved state before
        if (!mUserLearnedDrawer && !mFromSavedInstanc) {
            mdrawerLayout.openDrawer(containerView);
        }
        mdrawerLayout.setDrawerListener(actionBarDrawerToggle);
        //View.post is helpful when you don't have a direct access to the activity.
        //when called on Ui thread, Activity#runOnUiThread will call the run method directly, while View#post will post the runnable on the queue
        mdrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                // TODO Display the navigation drawer icon on action bar when there state has changed
                //ActionBarDrawerToggle.syncState is called properly offset[balance] this indicator[3 row icon] based on whether or not
                // the DrawerLayout is open or close after the instance state of the DrawerLayout has been restored.
                actionBarDrawerToggle.syncState(); //to appear 3 row icon to open close drawer
            }
        });

    }

    public static void saveToPreferences(Context context, String preferenceName, String preferenceValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(preferenceName, preferenceValue);
        //Commit() is instantaneous but performs disk writes. If you are on the ui thread you should call apply() which is asynchronous.
        editor.apply();
    }

    public static String readFromPreferences(Context context, String preferenceName, String defaultValue) {
        //The activity inherits the Context.
        //getApplicationContext().getSharedPreferences(*name*, *mode*);
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(preferenceName, defaultValue);
    }
}
