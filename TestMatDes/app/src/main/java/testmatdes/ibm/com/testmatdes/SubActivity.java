package testmatdes.ibm.com.testmatdes;

import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

public class SubActivity extends ActionBarActivity {

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        //for arrow in subactivity to appear
        //ActionBar actionBar = getSupportActionBar();
    //help you to add a back home key in your action bar.
        getSupportActionBar().setHomeButtonEnabled(true);// return actionBar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home){ //by default create id for this back button callde home
        // navigate Up to the appropriate parent using the NavUtils by create meta data in manifest and telling the activity his parent
        //navigateUpFromSameTask(Activity sourceActivity) When you call this method,it finishes the current activity and starts(or resumes)the appropriate parent activity
            NavUtils.navigateUpFromSameTask(this); //this = SubActivity
        }
        return super.onOptionsItemSelected(item);
    }
}
