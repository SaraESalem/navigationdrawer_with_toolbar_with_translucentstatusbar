package testmatdes.ibm.com.testmatdes;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.ViewConfiguration;

import java.lang.reflect.Field;

public class MainActivity extends ActionBarActivity {

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);////nav drawwer above toolbar
        //setContentView(R.layout.activity_main_appbar);//nav drawwer under toolbar
        //to force the 3 dots of action bar to appear
        makeActionOverflowMenuShown();

        //for toolbar & actionbar to appear in activity
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true); //true to show home icon

        //getSupportFragmentManager return FragmentManager  //to call fun setup
        NavigationDrawerFragment navigationDrawerFragment
                = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);

        //DrawerLayout/acts as a top-level container for window content that allows for interactive "drawer" views to be pulled out from the edge of the window.
        //Drawer positioning and layout is controlled using the android:layout_gravity
        navigationDrawerFragment.setUp(R.id.fragment_navigation_drawer,(DrawerLayout)findViewById(R.id.drawer_layout),toolbar); //t pass toolbar
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    //when press arrow
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //
        int id = item.getItemId();
        if(id == R.id.next){

            startActivity(new Intent(this,SubActivity.class));

        }
        return super.onOptionsItemSelected(item);
    }

    //to force the 3 dots of action bar to appear
    private void makeActionOverflowMenuShown() {
        //devices with hardware menu button (e.g. Samsung Note) don't show action overflow menu
        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception e) {
            Log.d("TAG", e.getLocalizedMessage());
        }
    }

}
